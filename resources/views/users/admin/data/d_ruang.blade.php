@section('title', 'Data Ruang')
@extends('layouts.master')
@section('content')
<div class="container-fluid">	
	<div class="row justify-content-center">
		<div class="col-sm-10">			
			<h4>Data Ruang</h4>
			<hr>
			<div class="row">
				<div class="col-sm-9">
					<a class="btn btn-sm btn-success" href="dataRuang/create">Buat Data Ruang Baru</a>
				</div>
				<div class="col-sm-3">
					<form action="/search" method="POST">
						@csrf
						<div class="input-group">
							<input name="path" class="form-control form-control-sm" value="{{ Request::path() }}" hidden>
							<input type="text" name="search" class="form-control form-control-sm" placeholder="Pencarian" required>&nbsp;
							<span class="input-group-btn">
							<input class="btn btn-sm btn-info" type="submit" name="" value="Cari">
							</span>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="row justify-content-center mt-2">
		<div class="col-sm-10">
			@if(Session::has('notif_danger'))
			<div class="alert alert-danger alert-dismissible fade show" role="alert">
				{!! session('notif_danger') !!}
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			@endif
			@if(Session::has('notif_success'))
			<div class="alert alert-success alert-dismissible fade show" role="alert">
				{!! session('notif_success') !!}
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			@endif
			@if(count($ruang) < 1)
			<div class="text-center">
				<h4 class="text">Data Tidak ada atau Kosong</h4>
			</div>			
			@else
			<div class="table-responsive">
				<table class="table table-bordered table-striped">
					<thead class="thead-dark">
						<tr>
							<th>#</th>
							<th>Nama Ruang</th>
							<th>Kode Ruang</th>
							<th>Keterangan</th>
							<th colspan="3">Tanggal Dibuat</th>
						</tr>
					</thead>
					<tbody>
						@foreach($ruang as $data)
						<tr>
							<td class="text-center">{{ $no++ }}</td>
							<td>{{ $data->nama_ruang }}</td>
							<td>{{ $data->kode_ruang }}</td>
							<td>{{ $data->keterangan }}</td>
							<td>{{ date('d-m-Y', strtotime($data->created_at)) }}</td>
							<td class="text-center"><a class="btn btn-sm btn-primary" href="dataRuang/{{ $data->id }}/update">Edit</a></td>
							<td class="text-center">
								<a data-confirm="{{ $data->nama_ruang }}" class="btn btn-sm btn-danger" href="dataRuang/{{ $data->id}}/delete">Hapus</a>
								{{-- <form action="dataRuang/{{ $data->id}}/delete" method="post">
									@csrf
									<input type="hidden" name="_method" value="DELETE">
									<input type="submit" name="" class="btn btn-sm btn-danger" value="Hapus">
								</form> --}}
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			
			@endif
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
			$('[data-confirm]').on('click', function(e){
        e.preventDefault(); //cancel default action

        //token
        var token = $('meta[name="csrf-token"]').attr('content');

        //Recuperate href value
        var href = $(this).attr('href');
        var message = $(this).data('confirm');

        //pop up
        swal({
        	title: "Apakah Anda Yakin?",
        	text: "Akan Dihapus Inventaris ["+message+"]",
        	icon: "warning",
        	buttons: ['Tidak', 'Ya'],
        	dangerMode: true,
        })
        .then((willDelete) => {
        	if (willDelete) {
        		swal("Data Akan Segera Dihapus.", {
        			icon: "info",
        		})
        		.then((ok) => {
        			if (ok) {
        				$.ajax({
        					url: href,
        					type: 'POST',
        					data: {'_method' : 'DELETE', '_token' : token},
        					success : function(data) {
        						window.location.reload();
        					},
        					error: function(jqXHR,error, errorThrown) {  
        						if (jqXHR.status&&jqXHR.status==500) {
        							var parse = jQuery.parseJSON(jqXHR.responseText);
        							swal({
        								title: "ERROR 500",
        								text: "Data Tidak Boleh Dihapus. Data Sedang Digunakan.",
        								icon: "warning",								        	
        								dangerMode: true,
        							});
        						} else if (jqXHR.status&&jqXHR.status==400) {
        							swal({
        								title: "ERROR 400",
        								text: "Data Tidak Boleh Dihapus. Data Sedang Digunakan.",
        								icon: "warning",								        	
        								dangerMode: true,
        							});
        						} else {
        							swal("Suatu Kesalahan Terjadi.", {
        								icon: 'info'
        							});
        						}
        					}
        				});
        			} else {
        				console.log(ok);
        			}        			
        		});        		  
        	} else {
        		swal("Penghapusan Data Dibatalkan.", {
        			icon: 'info'
        		});
        	}
        });
    });
		});
</script>
@endsection

